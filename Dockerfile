FROM docker.io/library/ruby:3.3.5-bookworm
RUN bundle config --global frozen 1
WORKDIR /usr/src/app
COPY Gemfile Gemfile.lock ./
RUN bundle install
RUN bundle exec yard gems
EXPOSE 8808/tcp
CMD ["bundle", "exec", "yard", "server", "--gems"]
